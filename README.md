# Zend Crud Imóveis

## Introdução

Criar uma área administrativa podendo utilizar um template admin
Implementar área de login, pesquisa por imóvel, cadastro, edição e exclusão do imóvel, importar imóvel via XML
 
Telas:
1. Tela de login
e-mail e senha
 
2. Tela de listagem dos imóveis com campo para busca
busca por id/código do imóvel
paginação para listagem do imóvel
 
3. Tela de cadastro/edição/exclusão do imóvel
Deve conter;
 
Título do imóvel
Código do imóvel
Tipo do imóvel (casa, apartamento)
 
Endereço do imóvel
Consumir uma API online que a partir do fornecimento do cep retorne as informações logradouro
Campos - CEP, Cidade, Estado, Bairro, Número, Complemento
 
Preço do imóvel
Informar valor do imóvel para Venda, Locação ou Temporada
 
Área do imóvel
metro quadrado
 
Quantidade de dormitórios, suítes, banheiros, salas e garagem
 
Descrição do imóvel
 
Imagem do imóvel
Upload de imagem do imóvel
 
4. Tela para importar imóveis consumidos via XML - http://imob21.com.br/acc/imob21/publish/integracao.xml
Importar somente as informações disponíveis no cadastro do imóvel
 
5. Versionamento
Versionar o projeto no github e enviar a url para avaliação.

## Instação

Após fazer download do projeto, utilizar composer para instalar dependências

```bash
$ composer install

Ao pedir para injetar dependências, sempre selecionar opção [1] config/modules.config.php
```

Adicionar um registro ao vhost, modificar Directory e DocumentRoot para diretorio correspondente ao Apache

```bash
<VirtualHost *:80>
    ServerName groupsoftware.local
     DocumentRoot C:/wamp64/www/zend-crud-imoveis/public
     SetEnv APPLICATION_ENV "development"
     <Directory C:/wamp64/www/zend-crud-imoveis/public>
         DirectoryIndex index.php
         AllowOverride All
         Order allow,deny
         Allow from all
     </Directory>
</VirtualHost>
```

Para upload de fotos, apache deve possuir permissão de escrita e leitura no path 

```bash
zend-crud-imoveis\public\foto
```

Editar arquivo hosts do Sistema

```bash
Abra a pasta %WinDir%\System32\Drivers\Etc (caso seja sistema Windows)
Abrir arquivo hosts
Adicionar a seguinte linha ao arquivo e salvar
127.0.0.1 groupsoftware.local
```
%WinDir%\System32\Drivers\Etc

Modificar variáveis de ambiente para upload de arquivos

```bash
file_uploads = On
post_max_size = 50M
upload_max_filesize = 50M
session.upload_progress.enabled = On
session.upload_progress.freq =  "1%"
session.upload_progress.min_freq = "1"
```

Rodar script no banco MySQL, cópia do script disponível no arquivo Migrate.sql

```bash
CREATE DATABASE `groupsoftware`;

USE `groupsoftware`;

CREATE TABLE `tipo_imovel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `tipo_imovel`(`id`,`tipo`) values (1,'Casa'),(2,'Apartamento');

CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `imovel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `tipo` int(10) unsigned DEFAULT NULL,
  `cep` varchar(10) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `numero` varchar(30) NOT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `preco_venda` decimal(15,2) DEFAULT NULL,
  `preco_locacao` decimal(15,2) DEFAULT NULL,
  `preco_temporada` decimal(15,2) DEFAULT NULL,
  `area` int(10) unsigned DEFAULT NULL,
  `dormitorio` int(10) unsigned DEFAULT NULL,
  `suite` int(10) unsigned DEFAULT NULL,
  `banheiro` int(10) unsigned DEFAULT NULL,
  `sala` int(10) unsigned DEFAULT NULL,
  `garagem` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_imovel` (`tipo`),
  CONSTRAINT `fk_tipo_imovel` FOREIGN KEY (`tipo`) REFERENCES `tipo_imovel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `foto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imovel_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_imovel_foto` (`imovel_id`),
  CONSTRAINT `fk_imovel_foto` FOREIGN KEY (`imovel_id`) REFERENCES `imovel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

Modificar arquivo zend-crud-imoveis\config\autoload\global.php com dados de acesso ao banco

```bash
 'host'     => '127.0.0.1',
 'user'     => 'root',
 'password' => '',
```

## Acessar Aplicação

```bash
Ao acessar aplicação pela primeira vez será criado um login padrão.
Fazer login utilizando email teste@teste.com e senha 123456

Para listagem com paginação dos Imóveis -> http://groupsoftware.local/imovel
Para importar arquivo XML de imóveis ->  http://groupsoftware.local/importar
Para adicinar um novo imóvel -> http://groupsoftware.local/adicionar
Para editar um imóvel -> http://groupsoftware.local/editar/idImovel
Para deletar um imóvel -> http://groupsoftware.local/deletar/idImovel
Para fazer logout -> http://groupsoftware.local/logout
```