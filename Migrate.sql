CREATE DATABASE `groupsoftware`;

USE `groupsoftware`;

CREATE TABLE `tipo_imovel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `tipo_imovel`(`id`,`tipo`) values (1,'Casa'),(2,'Apartamento');

CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `imovel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `tipo` int(10) unsigned DEFAULT NULL,
  `cep` varchar(10) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `numero` varchar(30) NOT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `preco_venda` decimal(15,2) DEFAULT NULL,
  `preco_locacao` decimal(15,2) DEFAULT NULL,
  `preco_temporada` decimal(15,2) DEFAULT NULL,
  `area` int(10) unsigned DEFAULT NULL,
  `dormitorio` int(10) unsigned DEFAULT NULL,
  `suite` int(10) unsigned DEFAULT NULL,
  `banheiro` int(10) unsigned DEFAULT NULL,
  `sala` int(10) unsigned DEFAULT NULL,
  `garagem` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_imovel` (`tipo`),
  CONSTRAINT `fk_tipo_imovel` FOREIGN KEY (`tipo`) REFERENCES `tipo_imovel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `foto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imovel_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_imovel_foto` (`imovel_id`),
  CONSTRAINT `fk_imovel_foto` FOREIGN KEY (`imovel_id`) REFERENCES `imovel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;