var imovel = {

    iniciar: function () {
        imovel.bindCEP();
        imovel.mascaras();
        imovel.excluirFoto();
    },

    cleanCEP: function () {
        $("#cidade").val("");
        $("#estado").val("");
        $("#complemento").val("");
        $("#bairro").val("");
    },

    excluirFoto: function () {
        $( ".remover-foto" ).click(function() {
            var fotoId = $(this).attr('foto-id');
            var jsonFotos = $('#jsonFotos').val();
            jsonFotos = jsonFotos + fotoId + ',';
            $('#jsonFotos').val(jsonFotos);
            $('#foto-'+fotoId).remove();
        });
    },

    bindCEP: function () {
        $("#cep").blur(function() {
            var cep = $(this).val().replace(/\D/g, '');

            if (cep != "") {
                var validacep = /^[0-9]{8}$/;
                if(validacep.test(cep)) {

                    $("#cidade").val("...");
                    $("#estado").val("...");
                    $("#complemento").val("...");
                    $("#bairro").val("...");

                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            $("#cidade").val(dados.localidade);
                            $("#estado").val(dados.uf);
                            $("#complemento").val(dados.complemento);
                            $("#bairro").val(dados.bairro);
                        } //end if.
                        else {
                            imovel.cleanCEP();
                            alert("CEP não encontrado.");
                        }
                    });
                }
                else {
                    imovel.cleanCEP();
                    alert("Formato de CEP inválido.");
                }
            }
            else {
                imovel.cleanCEP();
            }
        });
    },

    mascaras: function () {
        $("#cep").mask("99999-999");
    }
};

$(document).ready(function(){
    imovel.iniciar();
});
