<?php
namespace Admin;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'controllers' => [
        'factories' => [
            Controller\AdminController::class => Controller\Factory\AdminControllerFactory::class,
            Controller\ImovelController::class => Controller\Factory\ImovelControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            \Zend\Authentication\AuthenticationService::class => Service\Factory\AuthenticationServiceFactory::class,
            Service\AuthAdapter::class => Service\Factory\AuthAdapterFactory::class,
            Service\AuthManager::class => Service\Factory\AuthManagerFactory::class,
            Service\UsuarioManager::class => Service\Factory\UsuarioManagerFactory::class,
            Service\ImovelManager::class => Service\Factory\ImovelManagerFactory::class,
            Service\FotoManager::class => Service\Factory\FotoManagerFactory::class,
        ],
    ],

    'router' => [
        'routes' => [

            'editar' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/editar[[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ImovelController::class,
                        'action'        => 'edit',
                    ],
                ],
            ],

            'admin' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin[/:action]',
                    'defaults' => [
                        'controller'    => Controller\AdminController::class,
                        'action'        => 'index',
                    ],
                ],
            ],

            'imovel' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/imovel[/:action]',
                    'defaults' => [
                        'controller'    => Controller\ImovelController::class,
                        'action'        => 'index',
                    ],
                ],
            ],

            'deletar' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/deletar[[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ImovelController::class,
                        'action'        => 'deletar',
                    ],
                ],
            ],

            'login' => array(
                'type' => Literal::class,
                'options' => array(
                    'route' => '/login',
					'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'index',
                    ],
                ),
            ),

            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],

            'importar' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/importar',
                    'defaults' => [
                        'controller' => Controller\ImovelController::class,
                        'action'     => 'importar',
                    ],
                ],
            ],

            'adicionar' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/adicionar',
                    'defaults' => [
                        'controller' => Controller\ImovelController::class,
                        'action'     => 'add',
                    ],
                ],
            ],

        ],
    ],

    'access_filter' => [

        'controllers' => [
            Controller\AdminController::class => [
                ['actions' => ['login','logout'], 'allow' => '*']
            ],
            Controller\ImovelController::class => [
                ['actions' => ['index'], 'allow' => '@']
            ],
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            'Admin' => __DIR__ . '/../view',
        ],
    ],

    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],

];
