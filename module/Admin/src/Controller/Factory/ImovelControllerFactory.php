<?php
namespace Admin\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\ImovelController;
use Admin\Service\ImovelManager;
use Admin\Service\FotoManager;

/**
 * Injetar Dependencias
 */
class ImovelControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $imovelManager = $container->get(ImovelManager::class);
        $fotoManager = $container->get(FotoManager::class);

        return new ImovelController($entityManager, $imovelManager, $fotoManager);
    }
}