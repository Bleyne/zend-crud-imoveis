<?php
namespace Admin\Controller\Factory;

use Interop\Container\ContainerInterface;
use Admin\Controller\AdminController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Service\AuthManager;
use Admin\Service\UsuarioManager;

/**
 * Injetar Dependencias
 */
class AdminControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $authManager = $container->get(AuthManager::class);
        $userManager = $container->get(UsuarioManager::class);

        return new AdminController($entityManager, $authManager, $userManager);
    }
}
