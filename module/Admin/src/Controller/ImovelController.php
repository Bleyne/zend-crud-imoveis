<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Admin\Form\ImportarForm;
use Admin\Form\BuscaForm;
use Admin\Form\ImovelForm;
use Admin\Entity\Imovel;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;

class ImovelController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Imovel manager.
     * @var \Admin\Service\ImovelManager
     */
    private $imovelManager;

    /**
     * Foto manager.
     * @var \Admin\Service\FotoManager
     */
    private $fotoManager;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $imovelManager, $fotoManager)
    {
        $this->entityManager = $entityManager;
        $this->imovelManager = $imovelManager;
        $this->fotoManager = $fotoManager;
    }

    public function indexAction()
    {
        $form = new BuscaForm();

        $page = $this->params()->fromQuery('page', 1);

        $data = $this->params()->fromPost();

        $form->setData($data);

        if ($data) {
            if($form->isValid()) {
                $query = $this->entityManager->getRepository(Imovel::class)->findByTitulo(reset($data));
            }
        }
        else {
            $query = $this->entityManager->getRepository(Imovel::class)->findImoveis();
        }

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);



        return new ViewModel([
            'imoveis' => $paginator,
            'imovelManager' => $this->imovelManager,
            'form' => $form
        ]);
    }

    public function importarAction()
    {
        $form = new ImportarForm('upload-form');

        $request = $this->getRequest();
        if ($request->isPost()) {

            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $form->setData($post);
            if ($form->isValid()) {

                $xmlString = file_get_contents($post['xml-file']['tmp_name']);
                $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);

                foreach ($xml->Imoveis->Imovel as $row) {
                    $imovel = $this->imovelManager->addImovel($row);

                    foreach ($row->Fotos->Foto as $fotoImovel) {
                        $foto = $this->fotoManager->addFoto($imovel,$fotoImovel);
                    }

                }

                $this->flashMessenger()->setNamespace('success')->addMessage('Arquivo Importado');
            }
        }

        return array('form' => $form);
    }

    public function addAction()
    {
        $form = new ImovelForm();


        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {

                $data = $form->getData();

                $this->imovelManager->addImovelCrud($data,$_FILES['fotos']);

                $this->flashMessenger()->addMessage('Imóvel Cadastrado');

                return $this->redirect()->toRoute('imovel');
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $imovel = $this->entityManager->getRepository(Imovel::class)->find($id);

        if ($imovel == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $form = new ImovelForm($this->entityManager, $imovel);

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();

            $form->setData($data);

            if($form->isValid()) {

                $data = $form->getData();

                $this->imovelManager->updateImovel($imovel, $data,$_FILES['fotos']);

                $jsonFotos = explode(',',$data['jsonFotos']);

                array_pop($jsonFotos);

                $this->imovelManager->deleteFotoImovel($jsonFotos);

                $this->flashMessenger()->addMessage('Imóvel Atualizado');

                return $this->redirect()->toRoute('imovel');
            }
        } else {
            $form->setData(array(
                'id'=>$imovel->getId(),
                'titulo'=>$imovel->getTitulo(),
                'cep'=>$imovel->getCep(),
                'cidade'=>$imovel->getCidade(),
                'estado'=>$imovel->getEstado(),
                'bairro'=>$imovel->getBairro(),
                'numero'=>$imovel->getNumero(),
                'complemento'=>$imovel->getComplemento(),
                'preco_venda'=>$imovel->getPrecoVenda(),
                'preco_locacao'=>$imovel->getPrecoLocacao(),
                'preco_temporada'=>$imovel->getPrecoTemporada(),
                'area'=>$imovel->getArea(),
                'dormitorio'=>$imovel->getDormitorio(),
                'suite'=>$imovel->getSuite(),
                'banheiro'=>$imovel->getBanheiro(),
                'sala'=>$imovel->getSala(),
                'garagem'=>$imovel->getGaragem(),
                'tipo'=>$imovel->getTipo(),
            ));
        }

        return new ViewModel(array(
            'imovel' => $imovel,
            'form' => $form,
            'foto' => $imovel->getFoto()
        ));
    }

    public function deletarAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $imovel = $this->entityManager->getRepository(Imovel::class)->find($id);

        if ($imovel == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->imovelManager->deleteImovel($imovel);

        $this->flashMessenger()->addMessage('Imóvel Removido');

        return $this->redirect()->toRoute('imovel');
    }
}
