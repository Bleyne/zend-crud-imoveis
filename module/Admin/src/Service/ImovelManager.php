<?php
namespace Admin\Service;

use Admin\Entity\Foto;
use Admin\Entity\Imovel;
use Admin\Entity\TipoImovel;

/**
 * Service para administrar Imovel
 */
class ImovelManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $fotoManager;

    /**
     * Constructs the service.
     */
    public function __construct($entityManager,$fotoManager)
    {
        $this->entityManager = $entityManager;
        $this->fotoManager = $fotoManager;
    }
    
    /**
     * Adicionar novo Imovel
     */
    public function addImovel($data)
    {
        try {
            $imovel = new Imovel();

            $imovel->setTitulo(reset($data->CodigoImovel));

            $tipoImovel = $this->entityManager->getRepository(TipoImovel::class)
                ->findByTipo(reset($data->TipoImovel));

            if ($tipoImovel) {
                $imovel->setTipo(reset($tipoImovel));
            }

            $imovel->setCep(reset($data->CEP));

            $imovel->setCidade(reset($data->Cidade));

            $imovel->setEstado(reset($data->UF));

            $imovel->setBairro(reset($data->Bairro));

            $imovel->setNumero(reset($data->Numero));

            $imovel->setComplemento(reset($data->Complemento));

            /** Formatar Valor para padrão decimal 15,2*/
            $formatarPreco = function ($valor) {
                $valor = reset($valor);
                $valor = str_replace('.', '', $valor);
                return number_format((float)$valor, 2, '.', '');
            };

            $imovel->setPrecoVenda($formatarPreco($data->PrecoVenda));

            $imovel->setPrecoLocacao($formatarPreco($data->PrecoLocacao));

            $imovel->setPrecoTemporada($formatarPreco($data->PrecoLocacaoTemporada));

            $imovel->setArea(reset($data->AreaUtil));

            $imovel->setDormitorio(reset($data->QtdDormitorios));

            $imovel->setSuite(reset($data->QtdSuites));

            $imovel->setBanheiro(reset($data->QtdBanheiros));

            $imovel->setSala(reset($data->QtdSalas));

            $imovel->setGaragem(reset($data->QtdVagas));

            $this->entityManager->persist($imovel);

            $this->entityManager->flush();

            return $imovel;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function addImovelCrud($data,$fotos)
    {
        try {
            $imovel = new Imovel();

            $imovel->setTitulo($data['titulo']);

            $tipoImovel = $this->entityManager->getRepository(TipoImovel::class)
                ->find($data['tipo']);

            $imovel->setTipo($tipoImovel);

            $imovel->setCep($data['cep']);

            $imovel->setCidade($data['cidade']);

            $imovel->setEstado($data['estado']);

            $imovel->setBairro($data['bairro']);

            $imovel->setNumero($data['numero']);

            if ($data['complemento']) {
                $imovel->setComplemento($data['complemento']);
            }

            /** Formatar Valor para padrão decimal 10,2*/
            $formatarPreco = function ($valor) {
                $valor = str_replace('.', '', $valor);
                return number_format((float)$valor, 2, '.', '');
            };

            if ($data['preco_venda']) {
                $imovel->setPrecoVenda($formatarPreco($data['preco_venda']));
            }

            if ($data['preco_locacao']) {
                $imovel->setPrecoLocacao($formatarPreco($data['preco_locacao']));
            }

            if ($data['preco_temporada']) {
                $imovel->setPrecoTemporada($formatarPreco($data['preco_temporada']));
            }

            if ($data['area']) {
                $imovel->setArea($data['area']);
            }

            if ($data['dormitorio']) {
                $imovel->setDormitorio($data['dormitorio']);
            }

            if ($data['suite']) {
                $imovel->setSuite($data['suite']);
            }

            if ($data['banheiro']) {
                $imovel->setBanheiro($data['banheiro']);
            }

            if ($data['sala']) {
                $imovel->setSala($data['sala']);
            }

            if ($data['garagem']) {
                $imovel->setGaragem($data['garagem']);
            }


            if ($fotos) {
                foreach ($fotos['tmp_name'] as $key => $foto) {
                    if (!empty($foto)) {
                        $arquivoExt = explode(".",$fotos['name'][$key]);
                        $nomeArquivo = uniqid() . '.' . end($arquivoExt);
                        move_uploaded_file($foto, ROOT_PATH.'/public/foto/'.$nomeArquivo);
                        $url = '//'.$_SERVER['SERVER_NAME'] .'/foto/'.$nomeArquivo;
                        $fotoEntity = new Foto();
                        $fotoEntity->setImovel($imovel);
                        $fotoEntity->setUrl($url);
                        $imovel->addFoto($fotoEntity);
                    }
                }

            }

            $this->entityManager->persist($imovel);

            $this->entityManager->flush();

            return $imovel;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    /**
     * Atualizar Imovel
     */
    public function updateImovel($imovel, $data, $fotos)
    {
        $imovel->setTitulo($data['titulo']);

        $tipoImovel = $this->entityManager->getRepository(TipoImovel::class)
            ->find($data['tipo']);

        $imovel->setTipo($tipoImovel);

        $imovel->setCep($data['cep']);

        $imovel->setCidade($data['cidade']);

        $imovel->setEstado($data['estado']);

        $imovel->setBairro($data['bairro']);

        $imovel->setNumero($data['numero']);

        if ($data['complemento']) {
            $imovel->setComplemento($data['complemento']);
        }

        if ($data['preco_venda']) {
            $imovel->setPrecoVenda($data['preco_venda']);
        }

        if ($data['preco_locacao']) {
            $imovel->setPrecoLocacao($data['preco_locacao']);
        }

        if ($data['preco_temporada']) {
            $imovel->setPrecoTemporada($data['preco_temporada']);
        }

        if ($data['area']) {
            $imovel->setArea($data['area']);
        }

        if ($data['dormitorio']) {
            $imovel->setDormitorio($data['dormitorio']);
        }

        if ($data['suite']) {
            $imovel->setSuite($data['suite']);
        }

        if ($data['banheiro']) {
            $imovel->setBanheiro($data['banheiro']);
        }

        if ($data['sala']) {
            $imovel->setSala($data['sala']);
        }

        if ($data['garagem']) {
            $imovel->setGaragem($data['garagem']);
        }

        if ($fotos) {
            foreach ($fotos['tmp_name'] as $key => $foto) {
                if (!empty($foto)) {
                    $arquivoExt = explode(".",$fotos['name'][$key]);
                    $nomeArquivo = uniqid() . '.' . end($arquivoExt);
                    move_uploaded_file($foto, ROOT_PATH.'/public/foto/'.$nomeArquivo);
                    $url = '//'.$_SERVER['SERVER_NAME'] .'/foto/'.$nomeArquivo;
                    $fotoEntity = new Foto();
                    $fotoEntity->setImovel($imovel);
                    $fotoEntity->setUrl($url);
                    $imovel->addFoto($fotoEntity);
                }
            }

        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * Deletar Foto
     */
    public function deleteFotoImovel($fotos)
    {
        foreach ($fotos as $foto) {
            $fotoImovel = $this->entityManager->getRepository(Foto::class)->find($foto);
            $this->entityManager->remove($fotoImovel);
            $this->entityManager->flush();
        }
    }

    /**
     * Deletar Imovel
     */
    public function deleteImovel($imovel)
    {
        $this->entityManager->remove($imovel);
        $this->entityManager->flush();
    }
}

