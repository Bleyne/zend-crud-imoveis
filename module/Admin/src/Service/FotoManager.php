<?php
namespace Admin\Service;

use Admin\Entity\Foto;

/**
 * Service para administrar Fotos
 */
class FotoManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($entityManager) 
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * Adicionar Foto
     */
    public function addFoto($imovel, $data)
    {
        try {
            $foto = new Foto();

            $foto->setImovel($imovel);

            $foto->setUrl(reset($data->URLArquivo));

            $this->entityManager->persist($foto);

            $this->entityManager->flush();

            return $foto;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

