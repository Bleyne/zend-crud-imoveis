<?php
namespace Admin\Service;

use Admin\Entity\Usuario;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand;

/**
 * Service para administrar Usuários
 */
class UsuarioManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($entityManager) 
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Adicionar Primeiro Usuário
     */
    public function createAdminUserIfNotExists()
    {
        $user = $this->entityManager->getRepository(Usuario::class)->findOneBy([]);
        if ($user==null) {
            $user = new Usuario();
            $user->setEmail('teste@teste.com');
            $user->setNome('Admin');
            $bcrypt = new Bcrypt();
            $passwordHash = $bcrypt->create('123456');
            $user->setSenha($passwordHash);
            $user->setStatus(Usuario::STATUS_ACTIVE);
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}

