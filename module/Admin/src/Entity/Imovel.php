<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Imovel
 *
 * @ORM\Entity(repositoryClass="\Admin\Repository\ImovelRepository")
 * @ORM\Table(name="imovel", indexes={@ORM\Index(name="fk_tipo_imovel", columns={"tipo"})})
 */
class Imovel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=50, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=50, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=30, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=50, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_venda", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $precoVenda;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_locacao", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $precoLocacao;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_temporada", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $precoTemporada;

    /**
     * @var integer
     *
     * @ORM\Column(name="area", type="integer", nullable=true)
     */
    private $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="dormitorio", type="integer", nullable=true)
     */
    private $dormitorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="suite", type="integer", nullable=true)
     */
    private $suite;

    /**
     * @var integer
     *
     * @ORM\Column(name="banheiro", type="integer", nullable=true)
     */
    private $banheiro;

    /**
     * @var integer
     *
     * @ORM\Column(name="sala", type="integer", nullable=true)
     */
    private $sala;

    /**
     * @var integer
     *
     * @ORM\Column(name="garagem", type="integer", nullable=true)
     */
    private $garagem;

    /**
     * @var \TipoImovel
     *
     * @ORM\ManyToOne(targetEntity="TipoImovel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo", referencedColumnName="id")
     * })
     */
    private $tipo;

    /**
     * @var \Doctrine\Common\Collections\Collection | \Admin\Entity\Foto
     *
     * @ORM\OneToMany(targetEntity="Admin\Entity\Foto", mappedBy="imovel", cascade={"persist","remove"}, orphanRemoval=true)
     *
     **/
    protected $foto;

    /**
     * Turma constructor.
     */
    public function __construct()
    {
        $this->foto = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param string $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param string $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param string $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return string
     */
    public function getPrecoVenda()
    {
        return $this->precoVenda;
    }

    /**
     * @param string $precoVenda
     */
    public function setPrecoVenda($precoVenda)
    {
        $this->precoVenda = $precoVenda;
    }

    /**
     * @return string
     */
    public function getPrecoLocacao()
    {
        return $this->precoLocacao;
    }

    /**
     * @param string $precoLocacao
     */
    public function setPrecoLocacao($precoLocacao)
    {
        $this->precoLocacao = $precoLocacao;
    }

    /**
     * @return string
     */
    public function getPrecoTemporada()
    {
        return $this->precoTemporada;
    }

    /**
     * @param string $precoTemporada
     */
    public function setPrecoTemporada($precoTemporada)
    {
        $this->precoTemporada = $precoTemporada;
    }

    /**
     * @return int
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param int $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return int
     */
    public function getDormitorio()
    {
        return $this->dormitorio;
    }

    /**
     * @param int $dormitorio
     */
    public function setDormitorio($dormitorio)
    {
        $this->dormitorio = $dormitorio;
    }

    /**
     * @return int
     */
    public function getSuite()
    {
        return $this->suite;
    }

    /**
     * @param int $suite
     */
    public function setSuite($suite)
    {
        $this->suite = $suite;
    }

    /**
     * @return int
     */
    public function getBanheiro()
    {
        return $this->banheiro;
    }

    /**
     * @param int $banheiro
     */
    public function setBanheiro($banheiro)
    {
        $this->banheiro = $banheiro;
    }

    /**
     * @return int
     */
    public function getSala()
    {
        return $this->sala;
    }

    /**
     * @param int $sala
     */
    public function setSala($sala)
    {
        $this->sala = $sala;
    }

    /**
     * @return int
     */
    public function getGaragem()
    {
        return $this->garagem;
    }

    /**
     * @param int $garagem
     */
    public function setGaragem($garagem)
    {
        $this->garagem = $garagem;
    }

    /**
     * @return TipoImovel
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param TipoImovel $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return Foto|\Doctrine\Common\Collections\Collection
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param Foto|\Doctrine\Common\Collections\Collection $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * Insere Foto
     *
     * @param \Foto $foto
     */
    public function addFoto($foto)
    {
        if ($foto instanceof Foto) {

            if (is_null($foto->getId())) {
                $foto->setId($this);
            }
            $this->foto->add($foto);
        }
    }

    /**
     * Remove Foto
     *
     * @param ArrayCollection $foto
     */
    public function removeFoto($foto)
    {
        $this->foto->removeElement($foto);
    }


}

