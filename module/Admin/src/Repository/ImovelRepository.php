<?php
namespace Admin\Repository;

use Doctrine\ORM\EntityRepository;
use Admin\Entity\Imovel;

/**
 * Custom repository class para Imovel entity.
 */
class ImovelRepository extends EntityRepository
{
    /**
     * Query para recuperar todos os Imoveis
     * @return Query
     */
    public function findImoveis()
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        $queryBuilder->select('i')
            ->from(Imovel::class, 'i');
        
        return $queryBuilder->getQuery();
    }

    /**
     * Busca Imovel por titulo
     * @return Query
     */
    public function findByTitulo($titulo)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('i')
            ->from(Imovel::class, 'i')
            ->where('i.titulo = ?1')
            ->setParameter('1', $titulo);

        return $queryBuilder->getQuery();
    }
}