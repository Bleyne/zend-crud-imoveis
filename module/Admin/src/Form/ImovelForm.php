<?php
namespace Admin\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;


class ImovelForm extends Form
{
    /**
     * Entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;
    
    /**
     * Current user.
     * @var \Admin\Entity\Imovel
     */
    private $imovel = null;
    
    /**
     * Constructor.     
     */
    public function __construct($entityManager = null, $imovel = null)
    {
        parent::__construct('imovel-form');

        $this->setAttributes(array(
            'method' => 'post',
            'enctype' => "multipart/form-data"
        ));

        $this->entityManager = $entityManager;
        $this->imovel = $imovel;
        
        $this->addElements();
        $this->addInputFilter();          
    }

    /**
     * Adicionar elementos
     */
    protected function addElements() 
    {

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'jsonFotos',
            'attributes' => [
                'id' => 'jsonFotos',
            ],
        ));

        $this->add([            
            'type'  => 'text',
            'name' => 'titulo',
            'options' => [
                'label' => 'Título (Código Imóvel)',
            ],
            'attributes' => [
                'placeholder' => 'Título (Código Imóvel)',
                'class' => 'form-control',
                'id' => 'titulo',
            ],
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'tipo',
            'options' => [
                'label' => 'Tipo',
                'value_options' => [
                    1 => 'Casa',
                    2 => 'Apartamento',
                ]
            ],
            'attributes' => [
                'placeholder' => 'Tipo',
                'class' => 'form-control',
                'id' => 'tipo',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'cep',
            'options' => [
                'label' => 'CEP',
            ],
            'attributes' => [
                'placeholder' => 'CEP',
                'class' => 'form-control',
                'id' => 'cep',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'cidade',
            'options' => [
                'label' => 'Cidade',
            ],
            'attributes' => [
                'placeholder' => 'Cidade',
                'class' => 'form-control',
                'id' => 'cidade',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'estado',
            'options' => [
                'label' => 'Estado',
            ],
            'attributes' => [
                'placeholder' => 'Estado',
                'class' => 'form-control',
                'id' => 'estado',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'numero',
            'options' => [
                'label' => 'Numero',
            ],
            'attributes' => [
                'placeholder' => 'Numero',
                'class' => 'form-control',
                'id' => 'numero',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'complemento',
            'options' => [
                'label' => 'Complemento',
            ],
            'attributes' => [
                'placeholder' => 'Complemento',
                'class' => 'form-control',
                'id' => 'complemento',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bairro',
            'options' => [
                'label' => 'Bairro',
            ],
            'attributes' => [
                'placeholder' => 'Bairro',
                'class' => 'form-control',
                'id' => 'bairro',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'preco_venda',
            'options' => [
                'label' => 'Preco Venda',
            ],
            'attributes' => [
                'placeholder' => 'Preço Venda',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'preco_locacao',
            'options' => [
                'label' => 'Preco Locação',
            ],
            'attributes' => [
                'placeholder' => 'Preco Locação',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'preco_temporada',
            'options' => [
                'label' => 'Preco Temporada',
            ],
            'attributes' => [
                'placeholder' => 'Preco Temporada',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'area',
            'options' => [
                'label' => 'Área (m²)',
            ],
            'attributes' => [
                'placeholder' => 'Área (m²)',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'dormitorio',
            'options' => [
                'label' => 'Qtd Dormitorios',
            ],
            'attributes' => [
                'placeholder' => 'Qtd Dormitorios',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'suite',
            'options' => [
                'label' => 'Qtd Suites',
            ],
            'attributes' => [
                'placeholder' => 'Qtd Suites',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'banheiro',
            'options' => [
                'label' => 'Qtd Banheiros',
            ],
            'attributes' => [
                'placeholder' => 'Qtd Banheiros',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'sala',
            'options' => [
                'label' => 'Qtd Salas',
            ],
            'attributes' => [
                'placeholder' => 'Qtd Salas',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'garagem',
            'options' => [
                'label' => 'Qtd Garagens',
            ],
            'attributes' => [
                'placeholder' => 'Qtd Garagens',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class' => 'btn btn-primary',
                'value' => 'Enviar'
            ],
        ]);
    }
    
    /**
     * Adicionar Validações
     */
    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
                'name'     => 'titulo',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],                
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 255
                        ],
                    ],
                ],
            ]);     

        $inputFilter->add([
                'name'     => 'tipo',
                'required' => true,
                'filters'  => [                    
                    ['name' => 'StringTrim'],
                ],
            ]);

        $inputFilter->add([
            'name'     => 'cep',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 10
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'cidade',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 50
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'estado',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 50
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'bairro',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 50
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'numero',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 30
                    ],
                ],
            ],
        ]);

    }           
}