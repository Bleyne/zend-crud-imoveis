<?php
namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\FileInput;
use Zend\Form\Element;

/**
 * Form para importar arquivo XML
 */
class ImportarForm extends Form
{
    /**
     * Constructor.     
     */
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }
    
    /**
     * Adicionar Elementos
     */
    protected function addElements() 
    {
        // Add "file" campo
        $file = new Element\File('xml-file');
        $file->setLabel('XML Upload')
            ->setAttribute('id', 'xml-file');
        $this->add($file);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Enviar',
                'id' => 'submit',
            ],
        ]);
    }
    
    /**
     * Adicionar Validações
     */
    private function addInputFilter() 
    {

        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        $fileInput = new FileInput('xml-file');
        $fileInput->setRequired(true);

        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
    }        
}

