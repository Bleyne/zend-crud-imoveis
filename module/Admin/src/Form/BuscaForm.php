<?php
namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Formulario para busca de código na tabela de paginação
 */
class BuscaForm extends Form
{
    /**
     * Constructor.     
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('busca-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
                
        $this->addElements();
        $this->addInputFilter();          
    }

    /**
     * Adicionar elementos
     */
    protected function addElements() 
    {
        $this->add([            
            'type'  => 'text',
            'name' => 'busca',
            'attributes' => [
                'placeholder' => 'Código Imóvel',
                'class' => 'form-control input-md',
            ],
        ]);

    }
    
    /**
     * Adicionar Validações
     */
    private function addInputFilter() 
    {
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
                'name'     => 'busca',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 60
                        ],
                    ],
                ],
            ]);
    }        
}

